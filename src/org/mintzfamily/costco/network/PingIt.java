package org.mintzfamily.costco.network;

import java.io.IOException;
import java.net.InetAddress;

public class PingIt {
	private String IPAddress = "192.168.1.1";
	boolean isReachable = false;
	int timeout = 2000;

	public PingIt() {

	}

	public PingIt(String IPAddress) {
		this.IPAddress = IPAddress;
	}

	public boolean PingTest() {
		try {
			InetAddress adr = InetAddress.getByName(IPAddress);
			if (adr.isReachable(timeout)) {
				isReachable = true;
				System.out.println("Host Reachable: "
						+ adr.isReachable(timeout));

			} else {
				isReachable = false;
				System.out.println("Unreachable: " + IPAddress);
			}
		} catch (IOException ioe) {
			ioe.getMessage();
		}
		return isReachable;
	}

	public String getIPAddress() {
		return IPAddress;
	}
}