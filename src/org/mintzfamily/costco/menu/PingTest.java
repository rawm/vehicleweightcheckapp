package org.mintzfamily.costco.menu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JButton;

public class PingTest extends JFrame {

	private JPanel contentPane;
	private JTextField IPAddressField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PingTest frame = new PingTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PingTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblIpAddress = new JLabel("IP Address:");
		GridBagConstraints gbc_lblIpAddress = new GridBagConstraints();
		gbc_lblIpAddress.insets = new Insets(0, 0, 5, 5);
		gbc_lblIpAddress.anchor = GridBagConstraints.EAST;
		gbc_lblIpAddress.gridx = 1;
		gbc_lblIpAddress.gridy = 1;
		panel.add(lblIpAddress, gbc_lblIpAddress);
		
		IPAddressField = new JTextField();
		GridBagConstraints gbc_IPAddressField = new GridBagConstraints();
		gbc_IPAddressField.insets = new Insets(0, 0, 5, 0);
		gbc_IPAddressField.anchor = GridBagConstraints.WEST;
		gbc_IPAddressField.gridx = 2;
		gbc_IPAddressField.gridy = 1;
		panel.add(IPAddressField, gbc_IPAddressField);
		IPAddressField.setColumns(10);
		
		JButton btnTestConnection = new JButton("Test Connection");
		GridBagConstraints gbc_btnTestConnection = new GridBagConstraints();
		gbc_btnTestConnection.insets = new Insets(0, 0, 0, 5);
		gbc_btnTestConnection.gridx = 1;
		gbc_btnTestConnection.gridy = 3;
		panel.add(btnTestConnection, gbc_btnTestConnection);
	}

}
