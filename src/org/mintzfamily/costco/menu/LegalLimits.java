package org.mintzfamily.costco.menu;

public class LegalLimits {
	private float STEERMAX = 12000;
	private float DRIVEMAX = 34000;
	private float FRONTAXLEMAX = 19500;
	private float REARAXLEMAX = FRONTAXLEMAX;
	private float TRAILERTANDUMMAX = 34000;
	private float TOTAL = 80000;
	
	public float getSteeringMax(){
		return STEERMAX;
	}
	public float getDriveMax(){
		return DRIVEMAX;
	}
	
	public float getFrontAxleMax(){
		return FRONTAXLEMAX;
	}
	
	public float getRearAxleMax(){
		return REARAXLEMAX;
	}
	
	public float getTrailerTandumMax(){
		return TRAILERTANDUMMAX;
	}
	
	public float getGrandTotalMax(){
		return TOTAL;
	}

}
