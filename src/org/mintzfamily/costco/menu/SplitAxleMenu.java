package org.mintzfamily.costco.menu;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import java.awt.Toolkit;

import javax.swing.JTextArea;
import java.awt.SystemColor;

public class SplitAxleMenu extends JFrame {

	private JPanel contentPane;
	private JTextField steeringField;
	private JTextField driveField;
	private JTextField totalField;
	private JTextField frontAxleField;
	private JTextField rearAxleField;
	public float steeringS, driveS, frontAxle, rearAxle, total;
	
	public SplitAxleMenu() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(RegularTrailerMenu.class.getResource("/org/mintzfamily/costco/media/icon-partners-costco.png")));
		setTitle("SPLIT AXLE TRAILER");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 373, 540);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.WEST);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0};
		gbl_panel.rowHeights = new int[]{0};
		gbl_panel.columnWeights = new double[]{Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.EAST);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblSteering = new JLabel("Steering:");
		GridBagConstraints gbc_lblSteering = new GridBagConstraints();
		gbc_lblSteering.anchor = GridBagConstraints.EAST;
		gbc_lblSteering.insets = new Insets(0, 0, 5, 5);
		gbc_lblSteering.gridx = 0;
		gbc_lblSteering.gridy = 1;
		panel_1.add(lblSteering, gbc_lblSteering);
		
		steeringField = new JTextField();
		steeringField.setToolTipText("Number Values only");
		GridBagConstraints gbc_steeringField = new GridBagConstraints();
		gbc_steeringField.insets = new Insets(0, 0, 5, 5);
		gbc_steeringField.fill = GridBagConstraints.HORIZONTAL;
		gbc_steeringField.gridx = 1;
		gbc_steeringField.gridy = 1;
		panel_1.add(steeringField, gbc_steeringField);
		steeringField.setColumns(10);
		
		JLabel lblDrive = new JLabel("Drive:");
		GridBagConstraints gbc_lblDrive = new GridBagConstraints();
		gbc_lblDrive.anchor = GridBagConstraints.EAST;
		gbc_lblDrive.insets = new Insets(0, 0, 5, 5);
		gbc_lblDrive.gridx = 0;
		gbc_lblDrive.gridy = 2;
		panel_1.add(lblDrive, gbc_lblDrive);
		
		driveField = new JTextField();
		GridBagConstraints gbc_driveField = new GridBagConstraints();
		gbc_driveField.insets = new Insets(0, 0, 5, 5);
		gbc_driveField.fill = GridBagConstraints.HORIZONTAL;
		gbc_driveField.gridx = 1;
		gbc_driveField.gridy = 2;
		panel_1.add(driveField, gbc_driveField);
		driveField.setColumns(10);
		
		JLabel frontAxleLabel = new JLabel("Front Axle:");
		GridBagConstraints gbc_frontAxleLabel = new GridBagConstraints();
		gbc_frontAxleLabel.anchor = GridBagConstraints.EAST;
		gbc_frontAxleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_frontAxleLabel.gridx = 0;
		gbc_frontAxleLabel.gridy = 3;
		panel_1.add(frontAxleLabel, gbc_frontAxleLabel);
		
		frontAxleField = new JTextField();
		GridBagConstraints gbc_frontAxleField = new GridBagConstraints();
		gbc_frontAxleField.insets = new Insets(0, 0, 5, 5);
		gbc_frontAxleField.fill = GridBagConstraints.HORIZONTAL;
		gbc_frontAxleField.gridx = 1;
		gbc_frontAxleField.gridy = 3;
		panel_1.add(frontAxleField, gbc_frontAxleField);
		frontAxleField.setColumns(10);
		
		JLabel lblRearAxle = new JLabel("Rear Axle:");
		GridBagConstraints gbc_lblRearAxle = new GridBagConstraints();
		gbc_lblRearAxle.anchor = GridBagConstraints.EAST;
		gbc_lblRearAxle.insets = new Insets(0, 0, 5, 5);
		gbc_lblRearAxle.gridx = 0;
		gbc_lblRearAxle.gridy = 4;
		panel_1.add(lblRearAxle, gbc_lblRearAxle);
		
		rearAxleField = new JTextField();
		GridBagConstraints gbc_rearAxleField = new GridBagConstraints();
		gbc_rearAxleField.insets = new Insets(0, 0, 5, 5);
		gbc_rearAxleField.fill = GridBagConstraints.HORIZONTAL;
		gbc_rearAxleField.gridx = 1;
		gbc_rearAxleField.gridy = 4;
		panel_1.add(rearAxleField, gbc_rearAxleField);
		rearAxleField.setColumns(10);
		
		JLabel lblEstimatedTotal = new JLabel("Estimated Total");
		GridBagConstraints gbc_lblEstimatedTotal = new GridBagConstraints();
		gbc_lblEstimatedTotal.anchor = GridBagConstraints.EAST;
		gbc_lblEstimatedTotal.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstimatedTotal.gridx = 0;
		gbc_lblEstimatedTotal.gridy = 5;
		panel_1.add(lblEstimatedTotal, gbc_lblEstimatedTotal);
		
		totalField = new JTextField();
		totalField.setBackground(SystemColor.info);
		totalField.setEditable(false);
		GridBagConstraints gbc_totalField = new GridBagConstraints();
		gbc_totalField.insets = new Insets(0, 0, 5, 5);
		gbc_totalField.fill = GridBagConstraints.HORIZONTAL;
		gbc_totalField.gridx = 1;
		gbc_totalField.gridy = 5;
		panel_1.add(totalField, gbc_totalField);
		totalField.setColumns(10);
		
		JButton btnCalculateIt = new JButton("Calculate It");
		
		GridBagConstraints gbc_btnCalculateIt = new GridBagConstraints();
		gbc_btnCalculateIt.anchor = GridBagConstraints.WEST;
		gbc_btnCalculateIt.insets = new Insets(0, 0, 5, 5);
		gbc_btnCalculateIt.gridx = 1;
		gbc_btnCalculateIt.gridy = 7;
		panel_1.add(btnCalculateIt, gbc_btnCalculateIt);
		
		JButton clearButton = new JButton("Clear");
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				steeringField.setText("");
				driveField.setText("");
				totalField.setText("");
				frontAxleField.setText("");
				rearAxleField.setText("");
				
			}
		});
		GridBagConstraints gbc_clearButton = new GridBagConstraints();
		gbc_clearButton.anchor = GridBagConstraints.WEST;
		gbc_clearButton.insets = new Insets(0, 0, 5, 5);
		gbc_clearButton.gridx = 1;
		gbc_clearButton.gridy = 8;
		panel_1.add(clearButton, gbc_clearButton);
		
		final JTextArea infoTextArea = new JTextArea();
		infoTextArea.setBackground(SystemColor.activeCaptionBorder);
		infoTextArea.setLineWrap(true);
		infoTextArea.setText("Info");
		GridBagConstraints gbc_infoTextArea = new GridBagConstraints();
		gbc_infoTextArea.gridwidth = 4;
		gbc_infoTextArea.insets = new Insets(0, 0, 0, 5);
		gbc_infoTextArea.fill = GridBagConstraints.BOTH;
		gbc_infoTextArea.gridx = 0;
		gbc_infoTextArea.gridy = 11;
		panel_1.add(infoTextArea, gbc_infoTextArea);
	
		
	
		btnCalculateIt.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			LegalLimits ll = new LegalLimits();
			Kenworth1 k1 = new Kenworth1();
			
			try{
				steeringS = Float.parseFloat(steeringField.getText());
				steeringS += k1.getSteer();								// Adding the difference to steering.
				driveS = Float.parseFloat(driveField.getText());
				driveS += k1.getDrive();								//Adding the difference to Drive axles.
				frontAxle = Float.parseFloat(frontAxleField.getText());
				rearAxle = Float.parseFloat(rearAxleField.getText());
				total = steeringS + driveS + frontAxle + rearAxle;
			
				
				
				if(steeringS> ll.getSteeringMax()){
					JOptionPane.showMessageDialog(null, "Steering is too heavy by: " + 
							( steeringS - ll.getSteeringMax()), "alert", JOptionPane.ERROR_MESSAGE);
				}
				if(driveS > ll.getDriveMax()){
					JOptionPane.showMessageDialog(null, "Drive is too heavy by: " + 
							(driveS - ll.getDriveMax()),"alert", JOptionPane.ERROR_MESSAGE);
				}
				
				if(total > ll.getGrandTotalMax()){
					JOptionPane.showMessageDialog(null, "Total weight too heavy by: " +
							(total - ll.getGrandTotalMax()),"alert", JOptionPane.ERROR_MESSAGE);
				}
				else{
					infoTextArea.setText("Kenworth Actual weight:\n\n" + "Steering: " + steeringS +
							"\nDrive: " + driveS + "\nFront Axle:" + frontAxle + "\nRear Axle: " + rearAxle +
							"\nTotal: " + total);
					JOptionPane.showMessageDialog(null, "Everything looks good.\n Total weight: " + total);
				}
				totalField.setText("" + total);
				
			}catch(NumberFormatException nfe){
				System.out.println("You did not enter a number:" + 
						nfe.getMessage());
			}
			
		}
	});
		
		
	

	}
}
