package org.mintzfamily.costco.menu;

public interface Tractor {
	
	public float getSteer();
	public float getDrive();

}
