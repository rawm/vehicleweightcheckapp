/******************************************************************************************************
 * Program purpose:
 * 		To accurately apply the weight difference from Yard Hostler
 * 		tractor to actual Kenworth Costco Fleet Tractors.
 * 		Will be able to know whether there is a legal weight limit issue
 * 		or not with no confusion or guessing.
 * 
 * Author: Raymond Mintz (Prior Costco Fleet Driver Employee gone Programmer)
 * 
 * Date: April 2014
 * 
 * Contact: rbmintz.linuxadm@gmail.com
 * 			626-428-6851
 ****************************************************************************************************/

package org.mintzfamily.costco.menu;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class MenuChoice extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		MenuChoice mc = new MenuChoice();
		mc.setVisible(true);

	}

	public MenuChoice() {
		setTitle("Costco Fleet 960");
		setIconImage(Toolkit
				.getDefaultToolkit()
				.getImage(
						MenuChoice.class
								.getResource("/org/mintzfamily/costco/media/icon-partners-costco.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 572, 507);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel
				.setIcon(new ImageIcon(
						MenuChoice.class
								.getResource("/org/mintzfamily/costco/media/CostcoTractor.jpg")));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.gridheight = 4;
		gbc_lblNewLabel.gridwidth = 12;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		panel.add(lblNewLabel, gbc_lblNewLabel);

		JLabel lblChooseTrailerType = new JLabel("Choose Trailer Type:");
		GridBagConstraints gbc_lblChooseTrailerType = new GridBagConstraints();
		gbc_lblChooseTrailerType.insets = new Insets(0, 0, 5, 5);
		gbc_lblChooseTrailerType.gridx = 1;
		gbc_lblChooseTrailerType.gridy = 5;
		panel.add(lblChooseTrailerType, gbc_lblChooseTrailerType);

		JButton button = new JButton("SplitAxle");

		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 4;
		gbc_button.gridy = 6;
		panel.add(button, gbc_button);

		JButton button_1 = new JButton("Regular Trailer");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegularTrailerMenu rtm = new RegularTrailerMenu();
				rtm.setVisible(true);
			}
		});
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 5;
		gbc_button_1.gridy = 6;
		panel.add(button_1, gbc_button_1);

		JRadioButton radioButton = new JRadioButton("Yard Hostler");
		radioButton.setSelected(true);
		GridBagConstraints gbc_radioButton = new GridBagConstraints();
		gbc_radioButton.insets = new Insets(0, 0, 5, 5);
		gbc_radioButton.gridx = 5;
		gbc_radioButton.gridy = 7;
		panel.add(radioButton, gbc_radioButton);

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SplitAxleMenu sam = new SplitAxleMenu();
				sam.setVisible(true);

			}
		});

	}

}
