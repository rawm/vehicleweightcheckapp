package org.mintzfamily.costco.menu;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

import java.awt.Toolkit;

import javax.swing.JTextArea;
import java.awt.SystemColor;

public class RegularTrailerMenu extends JFrame {

	private JPanel contentPane;
	private JTextField steeringField;
	private JTextField driveField;
	private JTextField trailertandumField;
	private JTextField totalField;
	
	public float steering, drive, trailerTandum, total;

	
	public RegularTrailerMenu() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(RegularTrailerMenu.class.getResource("/org/mintzfamily/costco/media/icon-partners-costco.png")));
		setTitle("Regular Trailer\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 570, 540);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.WEST);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0};
		gbl_panel.rowHeights = new int[]{0};
		gbl_panel.columnWeights = new double[]{Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.EAST);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		JLabel lblSteering = new JLabel("Steering:");
		GridBagConstraints gbc_lblSteering = new GridBagConstraints();
		gbc_lblSteering.anchor = GridBagConstraints.EAST;
		gbc_lblSteering.insets = new Insets(0, 0, 5, 5);
		gbc_lblSteering.gridx = 0;
		gbc_lblSteering.gridy = 1;
		panel_1.add(lblSteering, gbc_lblSteering);
		
		steeringField = new JTextField();
		GridBagConstraints gbc_steeringField = new GridBagConstraints();
		gbc_steeringField.anchor = GridBagConstraints.WEST;
		gbc_steeringField.insets = new Insets(0, 0, 5, 5);
		gbc_steeringField.gridx = 1;
		gbc_steeringField.gridy = 1;
		panel_1.add(steeringField, gbc_steeringField);
		steeringField.setColumns(10);
		
		JLabel lblDrive = new JLabel("Drive:");
		GridBagConstraints gbc_lblDrive = new GridBagConstraints();
		gbc_lblDrive.anchor = GridBagConstraints.EAST;
		gbc_lblDrive.insets = new Insets(0, 0, 5, 5);
		gbc_lblDrive.gridx = 0;
		gbc_lblDrive.gridy = 2;
		panel_1.add(lblDrive, gbc_lblDrive);
		
		driveField = new JTextField();
		GridBagConstraints gbc_driveField = new GridBagConstraints();
		gbc_driveField.anchor = GridBagConstraints.WEST;
		gbc_driveField.insets = new Insets(0, 0, 5, 5);
		gbc_driveField.gridx = 1;
		gbc_driveField.gridy = 2;
		panel_1.add(driveField, gbc_driveField);
		driveField.setColumns(10);
		
		JLabel lblTrailerAxles = new JLabel("Trailer Axles:");
		GridBagConstraints gbc_lblTrailerAxles = new GridBagConstraints();
		gbc_lblTrailerAxles.anchor = GridBagConstraints.EAST;
		gbc_lblTrailerAxles.insets = new Insets(0, 0, 5, 5);
		gbc_lblTrailerAxles.gridx = 0;
		gbc_lblTrailerAxles.gridy = 3;
		panel_1.add(lblTrailerAxles, gbc_lblTrailerAxles);
		
		trailertandumField = new JTextField();
		GridBagConstraints gbc_trailertandumField = new GridBagConstraints();
		gbc_trailertandumField.anchor = GridBagConstraints.WEST;
		gbc_trailertandumField.insets = new Insets(0, 0, 5, 5);
		gbc_trailertandumField.gridx = 1;
		gbc_trailertandumField.gridy = 3;
		panel_1.add(trailertandumField, gbc_trailertandumField);
		trailertandumField.setColumns(10);
		
		JLabel lblEstimatedTotal = new JLabel("Estimated Total");
		GridBagConstraints gbc_lblEstimatedTotal = new GridBagConstraints();
		gbc_lblEstimatedTotal.anchor = GridBagConstraints.EAST;
		gbc_lblEstimatedTotal.insets = new Insets(0, 0, 5, 5);
		gbc_lblEstimatedTotal.gridx = 0;
		gbc_lblEstimatedTotal.gridy = 4;
		panel_1.add(lblEstimatedTotal, gbc_lblEstimatedTotal);
		
		totalField = new JTextField();
		totalField.setBackground(SystemColor.info);
		totalField.setEditable(false);
		GridBagConstraints gbc_totalField = new GridBagConstraints();
		gbc_totalField.anchor = GridBagConstraints.WEST;
		gbc_totalField.insets = new Insets(0, 0, 5, 5);
		gbc_totalField.gridx = 1;
		gbc_totalField.gridy = 4;
		panel_1.add(totalField, gbc_totalField);
		totalField.setColumns(10);
		
		JButton btnCalculateIt = new JButton("Calculate It");
		
		GridBagConstraints gbc_btnCalculateIt = new GridBagConstraints();
		gbc_btnCalculateIt.anchor = GridBagConstraints.WEST;
		gbc_btnCalculateIt.insets = new Insets(0, 0, 5, 5);
		gbc_btnCalculateIt.gridx = 1;
		gbc_btnCalculateIt.gridy = 5;
		panel_1.add(btnCalculateIt, gbc_btnCalculateIt);
		
		JButton clearButton = new JButton("Clear");
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				steeringField.setText("");
				driveField.setText("");
				trailertandumField.setText("");
				totalField.setText("");
			}
		});
		GridBagConstraints gbc_clearButton = new GridBagConstraints();
		gbc_clearButton.anchor = GridBagConstraints.WEST;
		gbc_clearButton.insets = new Insets(0, 0, 5, 5);
		gbc_clearButton.gridx = 1;
		gbc_clearButton.gridy = 6;
		panel_1.add(clearButton, gbc_clearButton);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(RegularTrailerMenu.class.getResource("/org/mintzfamily/costco/media/RegularTrailer.jpg")));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridwidth = 2;
		gbc_lblNewLabel.gridheight = 2;
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 9;
		panel_1.add(lblNewLabel, gbc_lblNewLabel);
		
		final JTextArea infoTextArea = new JTextArea();
		infoTextArea.setText("Info");
		GridBagConstraints gbc_infoTextArea = new GridBagConstraints();
		gbc_infoTextArea.insets = new Insets(0, 0, 0, 5);
		gbc_infoTextArea.fill = GridBagConstraints.BOTH;
		gbc_infoTextArea.gridx = 0;
		gbc_infoTextArea.gridy = 10;
		panel_1.add(infoTextArea, gbc_infoTextArea);
		
		
		/*
		 * This is where the program will determine whether the trailer passes weight limit laws after 
		 * adding the difference for specific tractor.
		 * 
		 */
		btnCalculateIt.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			LegalLimits ll = new LegalLimits();
			Kenworth1 k1 = new Kenworth1();
	
			try{
				
				
				steering = Float.parseFloat(steeringField.getText());
				steering += k1.getSteer();
				drive = Float.parseFloat(driveField.getText());
				drive += k1.getDrive();
				trailerTandum = Float.parseFloat(trailertandumField.getText());
				total = steering + drive + trailerTandum;
			
				if((steering < 0) || (drive < 0) || (trailerTandum < 0)){
					JOptionPane.showMessageDialog(null, "Values cannot be less than zero.", 
							"alert", JOptionPane.ERROR_MESSAGE, null);
					steeringField.setText(""); driveField.setText("");trailertandumField.setText("");
					totalField.setText("");
				}
				
				if(steering > ll.getSteeringMax()){
					JOptionPane.showMessageDialog(null, "Steering is too heavy by: " + 
							(steering - ll.getSteeringMax()));
				}
				if(drive > ll.getDriveMax()){
					JOptionPane.showMessageDialog(null, "Drive is too heavy by: " + 
							(ll.getSteeringMax() - drive));
				}
				
				if(total > ll.getGrandTotalMax()){
					JOptionPane.showMessageDialog(null, "Total weight too heavy by: " +
							(total - ll.getGrandTotalMax()));
				}
				
				else if(total < 0){
					JOptionPane.showMessageDialog(null, "Seriously?? total less than zero?", 
							"alert", JOptionPane.ERROR_MESSAGE, null);
					steeringField.setText(""); driveField.setText("");trailertandumField.setText("");
					totalField.setText("");
				}
				else
					JOptionPane.showMessageDialog(null, "Everything looks good.\n Total weight: " + total);
				infoTextArea.setText("Passes on Kenworth1\n\nHere\'s the diff:\n" + "Steering: " + steering + 
						"\nDrive: " + drive + "\nTrailer Axles: " + trailerTandum);
				
				
				totalField.setText("" + total);
				
				
				
			}catch(NumberFormatException nfe){
				JOptionPane.showMessageDialog(null, "You did not enter a number value.\n" + 
						nfe.getMessage());
			}
			
			
		}
	});
	
	

	}
}
