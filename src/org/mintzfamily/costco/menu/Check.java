package org.mintzfamily.costco.menu;
import javax.swing.JOptionPane;


public class Check {
	Check(){
		
	}
	
	public float calculate(float steerIn, float driveIn, float trailerTandumIn){
		Kenworth1 k1 = null;
		LegalLimits ll = null;
		
		//Add the difference to the axles with Tractor type Information.
		float steer = steerIn + k1.getSteer();
		float drive = driveIn + k1.getDrive();
		float total = steer + drive + trailerTandumIn;
		
		if(steer > ll.getSteeringMax()){
			JOptionPane.showMessageDialog(null, "Steering is too heavy by: " + 
					(ll.getSteeringMax() - steer));
		}
		if(drive > ll.getDriveMax()){
			JOptionPane.showMessageDialog(null, "Drive is too heavy by: " + 
					(ll.getDriveMax() - drive));
		}
		
		if(total > ll.getGrandTotalMax()){
			JOptionPane.showMessageDialog(null, "Total weight too heavy by: " +
					(ll.getGrandTotalMax() - total));
		}
		else
			JOptionPane.showMessageDialog(null, "Everything looks good.\n Total weight: " + total);
		return total;
	}
	
	public float calculate(float steerIn, float driveIn, float frontAxleIn, float rearAxleIn){
		Kenworth1 k1 = null;
		LegalLimits ll = null;
		
		//Add the difference to the axles with Tractor type Information.
		float steer = steerIn + k1.getSteer();
		float drive = driveIn + k1.getDrive();
		float rearAxle = rearAxleIn;
		float frontAxle = frontAxleIn;
		float total = steer + drive + frontAxle + rearAxle;
		
		if(steer > ll.getSteeringMax()){
			JOptionPane.showMessageDialog(null, "Steering is too heavy by: " + 
					(ll.getSteeringMax() - steer));
		}
		if(drive > ll.getDriveMax()){
			JOptionPane.showMessageDialog(null, "Drive is too heavy by: " + 
					(ll.getDriveMax() - drive));
		}
		if(frontAxle > ll.getFrontAxleMax()){
			JOptionPane.showMessageDialog(null, "Front split Axle too heavy by: \n" +
					(ll.getFrontAxleMax() - frontAxle));
		}
		
		if(rearAxle > ll.getRearAxleMax()){
			JOptionPane.showMessageDialog(null, "Rear split Axle too heavy by: \n" +
					(ll.getRearAxleMax() - rearAxle));
		}
		
		if(total > ll.getGrandTotalMax()){
			JOptionPane.showMessageDialog(null, "Total weight too heavy by: " +
					(ll.getGrandTotalMax() - total));
		}
		else
			JOptionPane.showMessageDialog(null, "Everything looks good.\n Total weight: " + total);
		return total;
	}
}
