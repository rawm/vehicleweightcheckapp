package org.mintzfamily.costco.menu;


public class Kenworth1 implements Tractor {
	
	/*
	 * This is the difference in pounds over the Yard Hostler Tractors
	 * These will be added to trailers weighed by the Yard Hostlers
	 * 
	 */
	private float STEER = 5000; 		
	private float DRIVE = 11000;


	public float getSteer(){
		return STEER;
	}
	
	public float getDrive(){
		return DRIVE;
	}
}
